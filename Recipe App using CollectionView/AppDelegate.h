//
//  AppDelegate.h
//  Recipe App using CollectionView
//
//  Created by Alex Brown on 6/11/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

