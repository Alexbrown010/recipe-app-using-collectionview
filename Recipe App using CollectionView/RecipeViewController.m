//
//  RecipeViewController.m
//  Recipe App using CollectionView
//
//  Created by Alex Brown on 6/11/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Set Background color
    UILabel *backgroundLbl = [[UILabel alloc]init];
    backgroundLbl.backgroundColor = [UIColor whiteColor];
    backgroundLbl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:backgroundLbl];
    
    //Background label
    NSArray* width0Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[backgroundLbl(1000)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(backgroundLbl)];
    
    NSArray* height0Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[backgroundLbl(1000)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(backgroundLbl)];
    
    [self.view addConstraints:width0Constraint];
    [self.view addConstraints:height0Constraint];
    
    //Recipe title lable
    UILabel* titleLbl = [[UILabel alloc]init];
    titleLbl.text = [self.infoDictionary objectForKey:@"Recipe"];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.font = [titleLbl.font fontWithSize:25];
    titleLbl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:titleLbl];
    
    //Recipe title label autolayout
    NSArray* widthConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[titleLbl(>=50)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLbl)];
    
    NSArray* heightConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLbl(>=150)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLbl)];
    
    NSArray* xConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[titleLbl]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLbl)];
    
    NSArray* yConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[titleLbl]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(titleLbl)];
    
    [self.view addConstraints:widthConstraint];
    [self.view addConstraints:heightConstraint];
    [self.view addConstraints:xConstraint];
    [self.view addConstraints:yConstraint];
    
    //Recipe imageView
    UIImageView* iv = [[UIImageView alloc]init];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    iv.translatesAutoresizingMaskIntoConstraints = NO;
    [iv setImage: [UIImage imageNamed: [self.infoDictionary objectForKey:@"Image"]]];
    [self.view addSubview:iv];
    
    //Recipe image autolayout
    
    NSArray* width1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[iv(>=200)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(iv)];
    
    NSArray* height1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[iv(>=150)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(iv)];
    
    NSArray* x1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[iv]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(iv)];
    
    NSArray* y1Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[iv]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(iv)];
    
    [self.view addConstraints:width1Constraint];
    [self.view addConstraints:height1Constraint];
    [self.view addConstraints:x1Constraint];
    [self.view addConstraints:y1Constraint];
    
    //Recipe description label
    UILabel* directionsTxt = [[UILabel alloc]init];
    directionsTxt.text = [self.infoDictionary objectForKey:@"Description"];
    directionsTxt.textAlignment = NSTextAlignmentCenter;
    directionsTxt.lineBreakMode = NSLineBreakByWordWrapping;
    directionsTxt.numberOfLines = 0;
    directionsTxt.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:directionsTxt];
    
    //Recipe description lable autolayout
    NSArray* width2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[directionsTxt(>=300)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(directionsTxt)];
    
    NSArray* height2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[directionsTxt(>=100)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(directionsTxt)];
    
    NSArray* x2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[directionsTxt]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(directionsTxt)];
    
    NSArray* y2Constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-250-[directionsTxt]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(directionsTxt)];
    
    [self.view addConstraints:width2Constraint];
    [self.view addConstraints:height2Constraint];
    [self.view addConstraints:x2Constraint];
    [self.view addConstraints:y2Constraint];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
