//
//  RecipeViewController.h
//  Recipe App using CollectionView
//
//  Created by Alex Brown on 6/11/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface RecipeViewController : ViewController
    @property (nonatomic, retain)
    NSDictionary* infoDictionary;
    @property (nonatomic, retain)
    NSArray *constraint_H;
    @property (nonatomic, retain)
    NSArray *constraint_V;
    @property (nonatomic, retain)
    NSDictionary *viewsDictionary;

@end
