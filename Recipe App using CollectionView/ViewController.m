//
//  ViewController.m
//  Recipe App using CollectionView
//
//  Created by Alex Brown on 6/11/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //PList
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    arrayOfInfo = [[NSArray alloc]initWithContentsOfFile:filePath];
    
    //Initialize UICollectionView
    UICollectionViewFlowLayout* vfl = [[UICollectionViewFlowLayout alloc]init];
    
    UICollectionView* cv = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height) collectionViewLayout:vfl];
    cv.delegate = self;
    cv.dataSource = self;
    [cv registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [self.view addSubview:cv];
}

//CollectionView cell
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary* infoDictionary = arrayOfInfo[indexPath.row];

    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, cell.bounds.size.width, cell.bounds.size.height)];
    [iv setImage: [UIImage imageNamed: [infoDictionary objectForKey:@"Image"]]];
    [cell.contentView addSubview:iv];
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, cell.bounds.size.width, 40)];
    title.tag = 200;
    title.font = [UIFont systemFontOfSize:25];
    title.text = [infoDictionary objectForKey:@"Recipe"];
    [cell.contentView addSubview:title];
    
    return cell;
}

//Number of cells in collectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

//CollectionView size
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width / 2 - 20, 200);
    
}

//CollectionView edge inserts
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 5);
}

//CollectionView select item at index path
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
        
    NSDictionary* infoDictionary = arrayOfInfo[indexPath.row];

    RecipeViewController* info = [RecipeViewController new];
    info.infoDictionary = infoDictionary;
    [self.navigationController pushViewController:info animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
